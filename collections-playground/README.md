# Collections Playground

This module is designed for me to play around with the variousw collections in
Java and check out various aspects of the APIs and how Jackson serializes the
data in those objects.

This project is neither intended nor designed to go through the normal
versioning or release CI/CD pipelines at MasterControl.

## Getting Started

Simply import the entire `sandbox` project into your favorite Java IDE and run
the `MapOfMapsTest` unit test class in the `src/test/java` folder
hierarchy.

Seriously, it's just **that** easy. The gradle build dependency system will
automatically build/compile the necessary Java class files and execute the
JUnit 5 tests.

## Why a module for Java Collections?

* This module is a playground for no reason other than to help me learn new
  things about the various collections classes in the Java language.
* The API for collections changed significantly when Java 5 introduced the
  concept of generics to the language.
* Then another big update to the collections APIs was introduced when lambdas
  were added in Java 8.
* Finally from Java 8 to Java 11, more significant updates have been made to
  the APIs.
* Sometimes it seems nearly impossible to keep up with all the changes
  happening so rapidly, and it's nice to have a place where I can write junk
  code to prove immutability of collections or JSON serialization of objects
  in collections, so I can more confidently write production-ready code.

## Current Unit Tests ##

The two tests in the `MapOfMapsTest` class currently prove two distinct
things:

1. How the Jackson library uses the `ObjectMapper` class to serialize the
   contents of a `Map` to a JSON-formatted String.
2. How modifying on element of a `Map` that is contained inside another `Map`
   changes the entire structure of the parent `Map` and the JSON-formatted
   output for that main `Map` as well as for the embedded `Map`.

I'm certain I'll add other test classes and other unit tests to this module
over time as other questions arise in the future.
