package com.mastercontrol.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * This class is specifically to test behavior around JSON serialization of Maps that contain Maps and modification of Map data that is
 * embedded in a parent Map object.
 */
public class MapOfMapsTest {
    private String expectedResult;
    private Map<String, Object> testMap;
    private ObjectMapper mapper;

    @BeforeEach
    void setup() {
        expectedResult = "{\"value1\":\"test string\",\"value2\":true,\"value3\":{\"sub1\":\"first value\","
                + "\"sub2\":\"second value\",\"sub3\":\"third value\"}}";
        final Map<String, String> embeddedMap = new HashMap<>(3);
        embeddedMap.put("sub1", "first value");
        embeddedMap.put("sub2", "second value");
        embeddedMap.put("sub3", "third value");
        this.testMap = Map.of("value1", "test string", "value2", true, "value3", embeddedMap);

        // The ObjectMapper from the Jackson library is how Java objects can be converted to String output.
        this.mapper = new ObjectMapper();
        /* Provides consistent ordering of keys in the generated String output - necessary for testing to assert equality in String
         * output, but normally not necessary in Production environments. */
        this.mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
    }

    @Test
    @DisplayName("Test Jackson serialization of Map<String, Object>")
    void testJacksonMapSerialization() throws JsonProcessingException {
        final String actual = this.mapper.writeValueAsString(this.testMap);
        assertEquals(expectedResult, actual);
    }

    @Test
    @DisplayName("Test modification of an embedded map")
    @SuppressWarnings("unchecked")
    void testEmbeddedMapModification() throws JsonProcessingException {
        String actual = mapper.writeValueAsString(this.testMap);
        assertEquals(expectedResult, actual);

        /* Normally the following line would be highlighted in a good Java IDE indicating the development tool
         * has no means by which to verify or validate the type of the explicit casting of the Object returned
         * from the call to Map#get(Object) that it is actually a Map<String,String>. Because of this, the
         * SuppressWarnings annotation is added to the definition of the method to ignore the reporting of such
         * type-checking issues.
         *
         * It is left as an exercise to the programmer to be certain that he or she is certain of which object
         * types are returned for specific keys in the Map. */
        final Map<String,String> embeddedMap = (Map<String,String>) this.testMap.get("value3");
        this.modifyEmbeddedMap(embeddedMap);

        final String expectedModifiedResult = "{\"value1\":\"test string\",\"value2\":true,\"value3\":{\"sub1\":\"modified value\","
                + "\"sub2\":\"second value\",\"sub3\":\"third value\"}}";
        actual = mapper.writeValueAsString(this.testMap);
        assertEquals(expectedModifiedResult, actual);
    }

    private void modifyEmbeddedMap(final Map<String, String> embedededMap) {
        /* This modification logic necessarily assumes three important things:
         *      1. That an embedded Map from some larger, encompassing Map is provided to this method.
         *      2. That the embedded Map is modifiable.
         *      3. That a Map entry with the key "sub1" already exists in the embedded Map with a value other
         *         than "modified value"
         * If these conditions are all true, then the subsequent generation of the encompassing Map will prove
         * that the Map#put(K, V) method invocation in this method effectively changed the data in the embedded
         * Map without having to recreate all the other data in the complex encompassing Map structure. */
        embedededMap.put("sub1", "modified value");
    }
}
