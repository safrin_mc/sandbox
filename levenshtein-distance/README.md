# Levenshtein Distance Module

For the hub and spoke feature, the Support group will need an efficient way to
look up a group (company name) and find the correct application to manage out
of all the Okta tenants. In order to do this, we needed an efficient mechanism
for providing a ranked search based on textual input from an MC Support
Engineer to find the closest matches to whatever the support person enters.

## Getting Started

Simply import the entire `sandbox` project into your favorite Java IDE and run
the `LevenshteinDistanceTest` and the `CaseSensitivityTest` unit test classes
in the `src/test/java` folder hierarchy of the `leveshtein-distance` module.

Seriously, it's just **that** easy. The gradle build dependency system will
automatically build/compile the necessary Java class files and execute the
JUnit 5 tests.

The `levenshtein-distance` module is already wired to write log statements
via the Log4J version 2 framework and the output will appear in the Console
appender, which is pretty standard. Debug level logging is enabled in the
current configuration so the person running the unit tests can see the
actual character sequences being compared along with the associated
calculated Levenshtein distance algorithm for each library. Info level
logging for each test will show the actual computational execution time
for using each algorithm tested.

## What is the Levenshtein Distance?

Some smart guy named Vladimir Levenshtein introduced the concept in 1965 of an
algorithm to describe the _distance_ between two character sequences. So as
an example, The Levenshtein distance between "fiddle" and "fiddle" is zero.
The two sequences match precisely.

The Levenshtein distance between "fiddle" and "faddle" is one since the two
character sequences are the exact same size and differ in only one exact
location. Explained more precisely, no characters need to be added or deleted
from the _source_ to get to the _target_ and only one letter needs to be
changed, which explains the Levenshtein distance calculated to be exactly
one.

"fiddle" and "saddle" would have a Levenshtein distance of two since the
two character sequences are the exact same size and differ in exactly two
specific locations. The position of the letters in the two character
sequences is irrelevant, but it does matter than exactly two letters needed
to be changed to get the _source_ to match the _target_. Again in this
example as in the first example, no characters need to be inserted or
deleted to get the two character sequences to match, so the Levenshtein
distance is exactly two between these two character sequences.

The ability fo quickly and accurately calculate the Levenshtein distance
between any two character sequences allows for the ability to calculate a
_ranked choice_ list of matching strings given a particular search string.
The challenges of calculating the Levenshtein distance are complex, though,
because it is inherently an expensive computational problem space and the
implementation to do it efficiently can be somewhat complex.

For more details about the Levenshtein distance, check out the
[Wikipedia article](https://en.wikipedia.org/wiki/Levenshtein_distance).

## Evaluated Libraries/Frameworks

Obviously writing this code is a problem because it introduces unnecessary
complexity into the codebase, and then it later becomes a maintenance problem
for future developers on the engineering team. For this type of problem, we
are much better served to consume a readily available, free implementation
maintained by other smart guys.

I used the low-hanging fruit (the easiest to find libraries available on the
Interwebz) for analysis to see how easy the APIs are to use and integrate with
our solution. The search was by no means comprehensive and there are very
likely other libraries that were never considered, but the following three
libraries seemed to be pretty good and also showed encouraging performance
numbers in very limited research and testing.

* Use of the
  [Apache `commons-text` library](https://commons.apache.org/proper/commons-text/)
  is governed by the
  [Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0).
  It is very easy to use and provides integers for the resulting Levenshtein
  distance that can be easy used to sort into a rank ordering of closely
  matching terms.
* Use of the
  [Apache Lucene framework](https://lucene.apache.org/) is also governed by
  the same
  [Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0).
  This framework/project is advertised as an open-source search framework
  that offers an ultra-fast search library. It was also very easy to use,
  but it provides a `float` result between the values of 0.0 and 1.0 to 
  indicate the distance between the two character sequences. It was
  non-intuitive at first because as values approach 1.0, they more closely
  match (smaller Levenshtein distance), and as the values approach 0.0, they
  are further apart in match (larger Levenshtein distance).
* Use of the
  [Mastfrog Technologies `util-strings` library](https://github.com/timboudreau/util/)
  is governed by the
  [MIT License](https://opensource.org/licenses/MIT). This library is
  developed and maintained by a smart guy named
  [Tim Boudreau](https://github.com/timboudreau). The algorithm he implemented
  for his `LevenshteinDistance` class seems to be a pretty standard solution
  that is mostly common found on the Internet. This algorithm performs nicely,
  but the Lucene library always seems to perform just a bit better.
  What I like about the Mastfrog implementation is the API is fairly complete
  and offers an ability to get the Levenshtein distance as either an `int`
  value or as a `float` value. Additionally, this was the only library that
  allowed for consideration of case-sensitivity (or not) when determining the
  Levenshtein distance between any two character sequences.

### Case-sensitivity

The [Mastfrog Technologies `util-strings` library](https://github.com/timboudreau/util/)
is the only implementation that actually discusses case-sensitivity
(and provides a way to disable it) in the API for calculating the Levenshtein
distance between the _source_ and _target_ character sequences.

The other two frameworks and APIs do not discuss it at all, but by default,
they do calculate the Levenshtein distance with consideration of case. This
means that the distance between an 'A' and an 'a' is considered maximal
distance and the distance between an 'a' and an 'a' or an 'A' and an 'A' is
minimal distance.

This is seriously important because it can significantly impact results when
looking for _closest_ matches to some provided String value from an end user.

If using either the Lucene or Apache Commons' implementation and
case-sensitivity should be disregarded, then the easiest and most obvious
approach is to convert the _source_ and _target_ character sequences to either
all lowercase or all uppercase letter character data (e.g.,
[String#toLowerCase()](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/String.html#toLowerCase())
or
[String#toUpperCase()](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/String.html#toUpperCase()))
before passing the character sequences into the APIs for Levenshtein
distance calculation.

## The Results

Ultimately my recommendation is to use the `LevenshteinDistance` class from
the Lucene library. It is just super-fast and efficient when compared with
the other two implementations. The fact that the returned `float` value was
on an inverse scale from what one would normally expect was a very minor,
trivial detail and can be easily accommodated in a `Comparator` for sorting
purposes.

Considering that the Lucene framework is developed and maintained by Apache
for the specific purpose of being a super-fast search engine that is
highly optimized to work with character data, it's not entirely surprising
that it performed better than the other two libraries. Plus it will likely
continue to be supported for a number of years in the future.

The second-best recommendation would be the Mastfrog Technologies
`util-strings` library. It offers a pretty comprehensive API loaded with
functionality and didn't perform too much worse than the Lucene framework.
The one caveat to watch out for with this library is that Mastfrog
Technologies seems to be this one guy,
[Tim Boudreau](https://github.com/timboudreau), and while he seems to be a
pretty smart guy and prolific on GitHub, it is not certain when he may stop
developing/supporting his `util-strings` library.

The Apache `commons-text` library was very easy to use and is the most
intuitive in terms of returning an `int` value that is what you would most
likely expect to see for a calculated Levenshtein distance, it also had the
worst performance of the three libraries that were evaluated. It seemed to
consistently run approximately 10 times longer on each test run than the
Lucene library's implementation.
