package com.mastercontrol.text.utils.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>Class to hold both the source and the target words as well as the calculated Levenshtein distance between
 * them. The <em>source</em> is a reference to the word that is given to the caller as a potential match.
 * The <em>target</em> is a reference to the word that is ideally being searched for. The Levenshtein distance
 * is a calculation of how close or far apart the two words are ... in other words, how many changes would be
 * needed to convert the <em>source</em> word to the <em>target</em> word.</p>
 * <p>It is worthwhile to note that different implementations and algorithms for computing the Levenshtein
 * distance can compute different values that have different interpretations. This class is not designed to
 * interpret the meaning of the Levenshtein distance value. That exercise is left to the caller based on
 * detailed knowledge of how the values were calculated and what they mean.</p>
 */
public class WeightedMatch {
    private static final Logger LOG = LoggerFactory.getLogger(WeightedMatch.class);

    private final float levenschteinDistance;
    private final String source, target;

    public WeightedMatch(final float levenschteinDistance, final String source, final String target) {
        this.levenschteinDistance = levenschteinDistance;
        this.source = source;
        this.target = target;
        LOG.debug("source = '{}', target = '{}', Levenshtein distance = {}", source, target, levenschteinDistance);
    }

    public float getLevenschteinDistance() {
        return this.levenschteinDistance;
    }

    public String getSource() {
        return this.source;
    }

    public String getTarget() {
        return this.target;
    }
}
