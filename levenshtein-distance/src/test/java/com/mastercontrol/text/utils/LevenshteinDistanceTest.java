package com.mastercontrol.text.utils;

import com.mastercontrol.text.utils.model.WeightedMatch;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.NumberFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests and times various implementation of the Levenshtein distance API from different libraries.
 */
public class LevenshteinDistanceTest {
    private static final Logger LOG = LoggerFactory.getLogger(LevenshteinDistanceTest.class);
    private static final NumberFormat INTEGER_FORMATTER = NumberFormat.getIntegerInstance();

    private static String STRING_1, STRING_2, TARGET;
    private static Set<String> SOURCES;
    private static List<String> EXPECTED;

    @BeforeAll
    static void setup() {
        STRING_1 = "This is a test sentence.";
        STRING_2 = "This is a different test sentence.";
        TARGET = "fiddle";
        SOURCES = Set.of("riddle", "fiddle", "joke", "straddle", "humor");
        EXPECTED = List.of("fiddle", "riddle", "straddle", "joke", "humor");
    }

    @Test
    @DisplayName("Test for the Apache Commons' Levenshtein distance calculation API")
    void testApacheLevenshteinDistanceClass() {
        final Comparator<WeightedMatch> byDistance = Comparator.comparing(wm -> wm.getLevenschteinDistance());
        final org.apache.commons.text.similarity.LevenshteinDistance calculator
                = new org.apache.commons.text.similarity.LevenshteinDistance();

        final long start = System.nanoTime();
        final List<String> actual =
                SOURCES.stream()
                       .map(source -> new WeightedMatch(calculator.apply(source, TARGET), source, TARGET))
                       .sorted(byDistance)
                       .map(wm -> wm.getSource())
                       .collect(Collectors.toUnmodifiableList());
        final long end = System.nanoTime();

        assertEquals(EXPECTED, actual);
        assertEquals(calculator.apply(STRING_1, STRING_2), calculator.apply(STRING_2, STRING_1));
        this.logExecutionTime("Apache commons-text", end - start);
    }

    @Test
    @DisplayName("Test for the Lucene framework's getDistance API")
    void testLuceneLevenshteinDistanceClass() {
        final org.apache.lucene.search.spell.LevenshteinDistance calculator
                = new org.apache.lucene.search.spell.LevenshteinDistance();

        /* Note that the Comparator is doing a one minus the calculated value to reverse the order of the returned
         * String values that most closely matched to the String values that were furthest apart from the target
         * String value. This is specific to the Lucene framework's implementation of the returned calculated
         * Levenstein distance value. */
        final Comparator<WeightedMatch> byDistance = Comparator.comparing(wm -> 1 - wm.getLevenschteinDistance());

        /* Observation: Fiddle and faddle both generate the same Levenshtein distance value as the all lowercase word
         *              fiddle. Consideration should be taken when multiple targets generate a value of 1.0 as in the
         *              target values fiddle, Fiddle, and FIDDLE would all generate a value of 1.0. */
        final long start = System.nanoTime();
        final List<String> actual =
                SOURCES.stream()
                       .map(source -> new WeightedMatch(
                               calculator.getDistance(source, TARGET), source, TARGET))
                       .sorted(byDistance)
                       .map(wm -> wm.getSource())
                       .collect(Collectors.toUnmodifiableList());
        final long end = System.nanoTime();

        assertEquals(EXPECTED, actual);
        assertEquals(calculator.getDistance(STRING_1, STRING_2), calculator.getDistance(STRING_2, STRING_1));
        this.logExecutionTime("Apache Lucene framework's", end - start);
    }

    @Test
    @DisplayName("Test for the Mastfrog Technologies' Levenshtein distance score API")
    void testMastfrogLevenshteinDistanceClass() {
        final Comparator<WeightedMatch> byDistance = Comparator.comparing(wm -> wm.getLevenschteinDistance());

        final long start = System.nanoTime();
        final List<String> actual =
                SOURCES.stream()
                       .map(source -> new WeightedMatch(
                               com.mastfrog.util.strings.LevenshteinDistance.score(source, TARGET, false), source, TARGET))
                       .sorted(byDistance)
                       .map(wm -> wm.getSource())
                       .collect(Collectors.toUnmodifiableList());
        final long end = System.nanoTime();

        assertEquals(EXPECTED, actual);
        assertEquals(com.mastfrog.util.strings.LevenshteinDistance.score(STRING_1, STRING_2, false),
                     com.mastfrog.util.strings.LevenshteinDistance.score(STRING_2, STRING_1, false));
        this.logExecutionTime("Mastfrog Technologies", end - start);
    }

    private void logExecutionTime(final String implementationName, final long totalNanoseconds) {
        LOG.info("Total processing time for the {} implementation was {} microseconds.", implementationName,
                 INTEGER_FORMATTER.format(Math.round(totalNanoseconds / 1000F)));
    }
}
