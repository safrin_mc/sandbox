package com.mastercontrol.text.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CaseSensitivityTest {
    /* Case sensitivity in this constant doesn't matter because that will be controlled with
     * String#toLowerCase() and String#toUpperCase() in individual unit tests. */
    private static final String TEST_STRING
            = "This is a test String for checking Levenshtein distance calculations with case sensitivity.";

    /* It would be normal or reasonable to think that the expected value for a complete non-match (every single
     * character in the sequence is a mismatch between source and target) should be 1.0, but remember that
     * String#toLowerCase() and String#toUpperCase() affect only letter character data in the character sequence
     * and have no impact on whitespace or punctuation character data, so the spaces and period in the testing
     * String value actually still match regardless of the case of the letter characters in the test String
     * object. */
    private static final double VIRTUAL_ONE = 0.8571428656578064;

    @Test
    @DisplayName("Test case-sensitive matching with the Apache Commons' Levenshtein distance apply API")
    void testApacheLevenshteinDistanceClass() {
        final org.apache.commons.text.similarity.LevenshteinDistance calculator
                = new org.apache.commons.text.similarity.LevenshteinDistance();
        /* The Apache commons-text framework's implementation of the Levenshtein distance calculation is the
         * only one that returns the Levenshtein distance value as an integer type, which is what most people
         * might expect to see or more intuitively understand as a result. The specific return value for this
         * unit test is most interesting because it strongly highlights the fact that the 12 whitespace
         * characters and the one punctuation character between the source and target character sequences are
         * unaffected (remain the same) after the String#toLowerCase() and String#toUpperCase() invocations. */
        assertEquals(78, calculator.apply(TEST_STRING.toLowerCase(), TEST_STRING.toUpperCase()));
        assertEquals(0, calculator.apply(TEST_STRING.toLowerCase(), TEST_STRING.toLowerCase()));
    }

    @Test
    @DisplayName("Test case-sensitive matching with the Lucene framework's getDistance API")
    void testCaseSensitivityWithLuceneLevenshteinDistanceClass() {
        final org.apache.lucene.search.spell.LevenshteinDistance calculator
                = new org.apache.lucene.search.spell.LevenshteinDistance();
        /* Remember that the Lucene framework's implementation of the Levenshtein distance calculation reverses
         * the normal or expected scale of the returning float value which is where 0.0 represents a complete
         * non-match (maximal distance between the source and target) and 1.0 represents a perfect match (minimal
         * distance between the source and target). */
        assertEquals(1 - VIRTUAL_ONE, calculator.getDistance(TEST_STRING.toLowerCase(), TEST_STRING.toUpperCase()));
        assertEquals(1.0, calculator.getDistance(TEST_STRING.toLowerCase(), TEST_STRING.toLowerCase()));
    }

    @Test
    @DisplayName("Test case-sensitive matching with the Mastfrog Technologies' Levenshtein distance score API")
    void testCaseSensitivityWithMastfrogLevenshteinDistanceClass() {
        /* The MastFrog Technologies' Levenshtein distance calculation API is the only one considered or examined
         * that allows a boolean value to indicate whether case-sensitivity should be considered when calculating
         * the Levenshtein distance between the source and target character sequences. */
        assertEquals(VIRTUAL_ONE, com.mastfrog.util.strings.LevenshteinDistance.score(
                TEST_STRING.toLowerCase(), TEST_STRING.toUpperCase(), true));
        assertEquals(0.0, com.mastfrog.util.strings.LevenshteinDistance.score(
                TEST_STRING.toLowerCase(), TEST_STRING.toUpperCase(), false));
    }
}
