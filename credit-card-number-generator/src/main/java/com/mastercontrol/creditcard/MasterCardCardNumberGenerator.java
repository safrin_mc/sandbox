package com.mastercontrol.creditcard;

public class MasterCardCardNumberGenerator extends CreditCardNumberGenerator {

    public MasterCardCardNumberGenerator() {
        super();
    }

    public MasterCardCardNumberGenerator(final long seed) {
        super(seed);
    }

    public String generateCardNumber() {
        final short[] digits = new short[16];
        final int startIndex = 2;
        digits[0] = 5;
        digits[1] = this.createSecondMasterCardDigit();

        super.createRemainingDigits(digits, startIndex);
        digits[digits.length - 1] = super.calculateCheckDigit(digits);

        return super.populateFormat(CreditCardType.MASTERCARD.getFormat(), digits);
    }

    private short createSecondMasterCardDigit() {
        return (short) (super.getRandomGenerator().nextInt(5) + 1);
    }
}
