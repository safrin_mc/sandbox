package com.mastercontrol.creditcard;

import com.mastercontrol.utils.ByteUtils;

import java.security.SecureRandom;

public abstract class CreditCardNumberGenerator {

    private final SecureRandom randomGenerator;

    CreditCardNumberGenerator() {
        this.randomGenerator = new SecureRandom(ByteUtils.longToBytes(System.nanoTime()));
    }

    CreditCardNumberGenerator(final long seed) {
        this.randomGenerator = new SecureRandom(ByteUtils.longToBytes(seed));
    }

    public abstract String generateCardNumber();

    SecureRandom getRandomGenerator() {
        return this.randomGenerator;
    }

    short[] createRemainingDigits(final short[] digits, final int startIndex) {
        for (int index = startIndex; index < digits.length - 2; index++) {
            digits[index] = (short) this.randomGenerator.nextInt(10);
        }

        return digits;
    }

    short calculateCheckDigit(final short[] digits) {
        int sum = 0;

        // Implementation of the Luhn algorithm
        for (int index = 0; index < digits.length - 2; index++) {
            final int multiplier = index % 2 == 0 ? 2 : 1;
            final int value = digits[index] * multiplier;

            if (value > 9) {
                final int rootDigit = 1 + value % 10;
                sum += rootDigit;
            } else {
                sum += value;
            }
        }

        return (short) ((10 - sum % 10) % 10);
    }

    String populateFormat(final String format, final short[] digits) {
        int index = 0;
        final StringBuilder str = new StringBuilder(format.length());

        for (int formatIndex = 0; formatIndex < format.length(); formatIndex++) {
            final char ch = format.charAt(formatIndex);

            if (ch == 'x') {
                if (index < digits.length) {
                    str.append(digits[index++]);
                } else {
                    str.append(ch);
                }
            } else {
                str.append(ch);
            }
        }

        return str.toString();
    }
}
