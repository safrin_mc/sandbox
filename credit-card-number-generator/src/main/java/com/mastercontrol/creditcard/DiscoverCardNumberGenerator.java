package com.mastercontrol.creditcard;

public class DiscoverCardNumberGenerator extends CreditCardNumberGenerator {

    public DiscoverCardNumberGenerator() {
        super();
    }

    public DiscoverCardNumberGenerator(final long seed) {
        super(seed);
    }

    public String generateCardNumber() {
        final short[] digits = new short[16];
        final int startIndex = 1;
        digits[0] = 6;

        super.createRemainingDigits(digits, startIndex);
        digits[digits.length - 1] = super.calculateCheckDigit(digits);

        return super.populateFormat(CreditCardType.DISCOVER.getFormat(), digits);
    }
}
