package com.mastercontrol.creditcard;

public class VisaCardNumberGenerator extends CreditCardNumberGenerator {

    public VisaCardNumberGenerator() {
        super();
    }

    public VisaCardNumberGenerator(final long seed) {
        super(seed);
    }

    public String generateCardNumber() {
        final short[] digits = new short[16];
        final int startIndex = 1;
        digits[0] = 4;

        super.createRemainingDigits(digits, startIndex);
        digits[digits.length - 1] = super.calculateCheckDigit(digits);

        return super.populateFormat(CreditCardType.VISA.getFormat(), digits);
    }
}
