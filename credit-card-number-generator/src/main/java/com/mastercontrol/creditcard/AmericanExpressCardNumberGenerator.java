package com.mastercontrol.creditcard;

public class AmericanExpressCardNumberGenerator extends CreditCardNumberGenerator {
    public AmericanExpressCardNumberGenerator() {
        super();
    }

    public AmericanExpressCardNumberGenerator(final long seed) {
        super(seed);
    }

    public String generateCardNumber() {
        final short[] digits = new short[15];
        final int startIndex = 2;
        digits[0] = 3;
        digits[1] = this.createSecondAmexDigit();

        super.createRemainingDigits(digits, startIndex);
        digits[digits.length - 1] = super.calculateCheckDigit(digits);

        return super.populateFormat(CreditCardType.AMEX.getFormat(), digits);
    }

    private short createSecondAmexDigit() {
        return (short) (super.getRandomGenerator().nextBoolean() ? 4 : 7);
    }
}
