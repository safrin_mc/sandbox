package com.mastercontrol.creditcard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(final String[] args) {
        final CreditCardNumberGenerator amexCardNumberGenerator = new AmericanExpressCardNumberGenerator();
        final CreditCardNumberGenerator discoverCardNumberGenerator = new DiscoverCardNumberGenerator();
        final CreditCardNumberGenerator masterCardCardNumberGenerator = new MasterCardCardNumberGenerator();
        final CreditCardNumberGenerator visaCardNumberGenerator = new VisaCardNumberGenerator();
        LOG.info("American Express card number {}", amexCardNumberGenerator.generateCardNumber());
        LOG.info("Visa card number {}", visaCardNumberGenerator.generateCardNumber());
        LOG.info("MasterCard card number {}", masterCardCardNumberGenerator.generateCardNumber());
        LOG.info("Discover card number {}", discoverCardNumberGenerator.generateCardNumber());
    }
}
