package com.mastercontrol.creditcard;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CreditCardNumberGeneratorTest {

    private static final long TEST_SEED = 123_456_789L;

    private static CreditCardNumberGenerator amexCardNumberGenerator;
    private static CreditCardNumberGenerator discoverCardNumberGenerator;
    private static CreditCardNumberGenerator masterCardCardNumberGenerator;
    private static CreditCardNumberGenerator visaCardNumberGenerator;

    @BeforeAll
    static void setup() {
        amexCardNumberGenerator = new AmericanExpressCardNumberGenerator(TEST_SEED);
        discoverCardNumberGenerator = new DiscoverCardNumberGenerator(TEST_SEED);
        masterCardCardNumberGenerator = new MasterCardCardNumberGenerator(TEST_SEED);
        visaCardNumberGenerator = new VisaCardNumberGenerator(TEST_SEED);
    }

    @ParameterizedTest(name="Test generateCardNumber for {0}")
    @MethodSource
    void testGenerateCardNumber(
            final String creditCardType,
            final CreditCardNumberGenerator generator,
            final String expectedResult) {
        assertEquals(expectedResult, generator.generateCardNumber());
    }

    private static final Stream<Arguments> testGenerateCardNumber() {
        return Stream.of(
                Arguments.of("American Express", amexCardNumberGenerator, "3479 072934 23205"),
                Arguments.of("Visa", visaCardNumberGenerator, "4166 0397 9250 7600"),
                Arguments.of("MasterCard", masterCardCardNumberGenerator, "5580 1839 4941 0402"),
                Arguments.of("Discover", discoverCardNumberGenerator, "6905 3750 7488 3009")
               );
    }
}