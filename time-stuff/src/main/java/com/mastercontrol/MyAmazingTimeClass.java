package com.mastercontrol;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.time.Clock;
import java.time.Instant;

@Slf4j
public class MyAmazingTimeClass {
    private final Clock clock;

    public MyAmazingTimeClass(@NonNull final Clock clock) {
        this.clock = clock;
    }

    // Default constructor
    public MyAmazingTimeClass() {
        this(Clock.systemUTC());
    }

    public Instant getCurrentTime() {
        // Simulate some kind of processing
        try {
            Thread.sleep(10);
        } catch (InterruptedException ex) {
            log.info("This thread was interrupted", ex);
        }

        return this.clock.instant();
    }
}
