package com.mastercontrol;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MyAmazingTimeClassTest {
    private static final Logger log = LoggerFactory.getLogger(MyAmazingTimeClassTest.class);
    private static final DateTimeFormatter FORMATTER
            = DateTimeFormatter.ofPattern("EEE., MMM. d, yyyy 'at' h:mm:ss.SSS a zzz", Locale.US);
    private static final Instant MY_TIME = LocalDateTime.of(2021, 10, 1, 12, 0, 0)
                                                        .toInstant(ZoneOffset.UTC);

    private MyAmazingTimeClass testy;

    @BeforeEach
    void setup() {
        final var fixedClock = Clock.fixed(MY_TIME, ZoneOffset.UTC);
        this.testy = new MyAmazingTimeClass(fixedClock);
    }

    @Test
    void testGetCurrentTime() {
        final var now = Clock.systemDefaultZone().instant();
        log.info("Now is {}", FORMATTER.format(ZonedDateTime.ofInstant(MY_TIME, ZoneOffset.UTC)));
        assertEquals(MY_TIME, testy.getCurrentTime());
    }
}
