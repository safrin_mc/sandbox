package sequences;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AnalyzerTest {

    @ParameterizedTest(name = "Test largestVariance for input String ''{1}''")
    @MethodSource
    void largestVariance(final int expectedVariance, final String inputString) {
        assertEquals(expectedVariance, Analyzer.largestVariance(inputString));
    }

    private static Stream<Arguments> largestVariance() {
        return Stream.of(
                Arguments.of(0, "ab"),
                Arguments.of(1, "abab"),
                Arguments.of(2, "ababaabb"),
                Arguments.of(4, "aaababaabb")
        );
    }
}