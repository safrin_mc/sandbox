package com.mastercontrol.utilities.models;

import lombok.Value;

import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@Value
public class Duplicate {
    String key;
    Set<Long> lines;

    public Duplicate(final String key, final Set<Long> lineNumbers) {
        this.key = key;
        final SortedSet<Long> copy = new TreeSet<>(lineNumbers);
        this.lines = Collections.unmodifiableSortedSet(copy);
    }
}
