package com.mastercontrol.utilities.models;

import lombok.Value;

@Value
public class Key {
    String key;
    Long lineNumber;
}
