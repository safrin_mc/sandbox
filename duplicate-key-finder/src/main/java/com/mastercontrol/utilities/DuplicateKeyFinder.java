package com.mastercontrol.utilities;

import com.mastercontrol.utilities.models.Duplicate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Set;

/**
 * <p>This class is the main program and entry point for the JVM.</p>
 * <p>Exit codes:</p>
 * <ol>
 *     <li>No valid filename argument provided</li>
 *     <li>A null value was somehow provided for the filename - should not be possible to happen</li>
 *     <li>The specified file does not exist</li>
 *     <li>the user does not have read permissions for the file</li>
 *     <li>The specified file is not a text file</li>
 * </ol>
 */
public class DuplicateKeyFinder {

    private static final int NO_FILENAME_ERROR = 1;
    private static final int NULL_FILENAME_ERROR = 2;
    private static final int FILE_DOES_NOT_EXIST_ERROR = 3;
    private static final int USER_DOES_NOT_HAVE_READ_PERMISSION_ERROR = 4;
    private static final int FILE_IS_NOT_PLAIN_TEXT_ERROR = 5;

    private final File file;

    private DuplicateKeyFinder(final File file) {
        this.file = file;
    }

    private Set<Duplicate> findDuplicateKeys() {
        try (BufferedReader reader = new BufferedReader(new FileReader(this.file))) {
            String line = reader.readLine();
            while (line != null) {
                System.out.println(line);
                // read next line
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(final String[] args) throws IOException {
        if (args.length == 0) {
            System.exit(NO_FILENAME_ERROR);
        }

        final File file = validateTextFile(args[0]);

        final DuplicateKeyFinder finder = new DuplicateKeyFinder(file);
        final Set<Duplicate> duplicates = finder.findDuplicateKeys();

        if (duplicates.isEmpty()) {
            System.out.println(String.format("There are no duplicate keys in %s", file.getCanonicalPath()));
        } else {
            duplicates.stream()
                    .forEach(dup -> displayResult(dup));
        }
    }

    private static File validateTextFile(final String filename) {
        final File file;

        try {
            file = new File(filename);

            if (!file.exists()) {
                System.exit(FILE_DOES_NOT_EXIST_ERROR);
            }

            if (!file.canRead()) {
                System.exit(USER_DOES_NOT_HAVE_READ_PERMISSION_ERROR);
            }

            if (!Files.probeContentType(file.toPath()).equals("text")) {
                System.exit(FILE_IS_NOT_PLAIN_TEXT_ERROR);
            }

            return file;
        } catch(NullPointerException ex) {
            System.exit(NULL_FILENAME_ERROR);
        } catch (IOException ex) {
            System.exit(FILE_IS_NOT_PLAIN_TEXT_ERROR);
        }

        return null;
    }

    private static void displayResult(final Duplicate duplicate) {
        System.out.println(String.format("Key '%s' was found at lines %s",
                                         duplicate.getKey(),
                                         duplicate.getLineNumbers()));
    }
}
