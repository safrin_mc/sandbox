# README #

This repository is **not** intended to be a normal project for an application.
This project is designed to have modules added for exploring new, various
technologies or language features.

This project is neither intended nor designed to go through the normal
versioning or release CI/CD pipelines at MasterControl.

## What is this repository for? ##

* This is a sandbox for no reason other than to help me learn new technologies,
  frameworks, libraries, and Java language features.
* Each module should represent a new concept or related group of concepts that
  I either spent time learning or am currently learning.
* Some modules of this project may well be a vetting ground or proof of concept
  for how to achieve some particular objective or implementation.

## How do I get set up? ##

1. Clone this repository to your local dev environment.
2. Open the project in your favorite IDE (I prefer the IntelliJ IDEA myself.)
3. Explore the modules and run whatever unit tests are available or any
   classes that have a `public static void main(String[] args)` method.
4. Any special instructions to configure a database or other such specialized
   deployment or configuration requirements will be documented in the specific
   module that has those requirements.

## Contribution guidelines ##

This project is not really intended for others to contribute, but other
engineers are more than welcome to fork from any branch of this repository
and do your own exploration.

If any MasterControl engineers have any suggestions or enhancements for
experiments on any module in this repository, I strongly encourage and request
them to contact me directly so I can learn from the suggestions and making any
suggested enhancements myself.

## Who do I talk to? ##

* This repository is intended to be public for all engineers at MasterControl
  to access and play around with
* Questions regarding any of the modules, libraries, or technologies used in
  this project should be directed to Steve Afrin,
  [safrin@mastercontrol.com](mailto:safrin@mastercontrol.com)
