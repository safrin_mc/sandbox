package mastercontrol;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MyAmazingClassTest {
    MyAmazingClass myAmazingObject;

    @BeforeEach
    void setup() {
        this.myAmazingObject = new MyAmazingClass();
    }

    @ParameterizedTest(name = "Test checkEmptyBlankOrNull - {0}")
    @MethodSource("stringTestSource")
    void testCheckEmptyBlankOrNull(final String description, final String expectedResponse, final String testValue) {
        if (expectedResponse == null) {
            var thrown = assertThrows(IllegalArgumentException.class,
                                      () -> this.myAmazingObject.checkEmptyBlankOrNull(testValue));
            assertEquals("The string value cannot be blank, empty, or null", thrown.getMessage());
        } else {
            assertEquals(expectedResponse, this.myAmazingObject.checkEmptyBlankOrNull(testValue));
        }
    }

    private static Stream<Arguments> stringTestSource() {
        return Stream.of(
                Arguments.of("null", null, null),
                Arguments.of("empty String", null, ""),
                Arguments.of("blank String", null, "   "),
                Arguments.of("valid String value", "test", "test"));
    }
}