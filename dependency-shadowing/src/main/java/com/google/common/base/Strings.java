package com.google.common.base;

public class Strings {
    public String reverse(final String str) {
        return new StringBuilder(str).reverse().toString();
    }
}
