package com.mastercontrol.shaded.google.common.base;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Strings {
    public static String nullToEmpty(final String str) {
        if (str == null) {
            log.info("The string value is null");
            return "";
        } else {
            if (str.isEmpty()) {
                log.info("The string value is empty");
            } else if (str.isBlank()) {
                log.info("The string value is blank");
            } else {
                log.info("The string value is '{}'", str);
            }

            return str;
        }
    }
}
