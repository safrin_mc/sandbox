package mastercontrol;

import com.google.common.base.Strings;

public class MyAmazingClass {
    public String checkEmptyBlankOrNull(final String str) {
        if (Strings.nullToEmpty(str).trim().isBlank()) {
            throw new IllegalArgumentException("The string value cannot be blank, empty, or null");
        }

        return str;
    }
}
