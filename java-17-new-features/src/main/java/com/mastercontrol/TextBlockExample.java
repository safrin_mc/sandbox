package com.mastercontrol;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastercontrol.models.InventoryManifest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TextBlockExample {
    private static final ObjectMapper JSON_MAPPER = new ObjectMapper();

    public static void main(final String[] args) {
        final var json = """
                {
                  "sourceBucket" : "trunk-us-west-2",
                  "destinationBucket" : "arn:aws:s3:::trunk-us-west-2",
                  "version" : "2016-11-30",
                  "creationTimestamp" : "1634169600000",
                  "fileFormat" : "ORC",
                  "fileSchema" : "struct<bucket:string,key:string,size:bigint>",
                  "files" : [ {
                    "key" : "trunk-us-west-2/storage-trends/data/7813357e-9e80-4ab5-9248-345624349803.orc",
                    "size" : 13330592,
                    "MD5checksum" : "fe1d579822cf3af498c3bcb4ab9acb31"
                  }, {
                    "key" : "trunk-us-west-2/storage-trends/data/f2841bcb-90ee-4afe-a0bc-eae5f54f6195.orc",
                    "size" : 17699909,
                    "MD5checksum" : "227d50323fb56962c2bc6e7a58b80020"
                  }, {
                    "key" : "trunk-us-west-2/storage-trends/data/2f41fff5-fb90-4110-848e-042ebc7a2bc8.orc",
                    "size" : 55019,
                    "MD5checksum" : "6bcc8a81c505d6137f1eadfd784024a6"
                  } ]
                }
                """;
        try {
            final var manifest = JSON_MAPPER.readValue(json, InventoryManifest.class);
            System.out.printf("This manifest is for an S3 inventory with output format set to %s%n",
                              manifest.getFileFormat());
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
        }
    }
}
