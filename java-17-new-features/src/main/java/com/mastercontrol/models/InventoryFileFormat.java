package com.mastercontrol.models;

import java.util.Arrays;

public enum InventoryFileFormat {
    CSV, ORC, PARQUET;

    public static InventoryFileFormat findName(final String name) {
        return Arrays.stream(values())
              .filter(f -> f.toString().equalsIgnoreCase(name))
              .findFirst()
              .orElse(null);
    }
}
