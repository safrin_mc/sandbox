package com.mastercontrol.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mastercontrol.converters.StringToInstantConverter;
import com.mastercontrol.converters.StringToInventoryFileFormatConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.time.Instant;
import java.util.List;

@Builder
@Jacksonized
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@AllArgsConstructor
public class InventoryManifest {
    @JsonProperty("creationTimestamp")
    @JsonDeserialize(converter = StringToInstantConverter.class)
    private final Instant creationTime;

    @JsonProperty("fileFormat")
    @JsonDeserialize(converter = StringToInventoryFileFormatConverter.class)
    private final InventoryFileFormat fileFormat;

    @JsonProperty("files")
    private final List<InventoryDataFile> files;
}
