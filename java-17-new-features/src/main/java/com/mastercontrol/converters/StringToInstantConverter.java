package com.mastercontrol.converters;

import com.fasterxml.jackson.databind.util.StdConverter;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;

@Slf4j
public class StringToInstantConverter extends StdConverter<String, Instant> {
    @Override
    public Instant convert(String value) {
        return Instant.ofEpochMilli(Long.parseLong(value));
    }
}
