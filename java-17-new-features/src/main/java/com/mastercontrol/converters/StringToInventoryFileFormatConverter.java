package com.mastercontrol.converters;

import com.fasterxml.jackson.databind.util.StdConverter;
import com.mastercontrol.models.InventoryFileFormat;

public class StringToInventoryFileFormatConverter extends StdConverter<String, InventoryFileFormat> {
    @Override
    public InventoryFileFormat convert(String value) {
        return InventoryFileFormat.findName(value);
    }
}
